package com.ltfs.farm.scripts;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.ltfs.farm.pageobjects.AssetDetailsPage;
import com.ltfs.farm.pageobjects.SelectDealerPage;
import com.ltfs.generics.BaseLib;
import com.ltfs.generics.ConfigDataProvider;
import com.ltfs.generics.ExtentReportsClass;
import com.ltfs.generics.SwipeOperation;

public class AssetDetailsTest extends BaseLib{
	
	Logger log = Logger.getLogger(AssetDetailsTest.class);	
	ExtentReportsClass erc;
	
	@Test(dataProviderClass=ConfigDataProvider.class, dataProvider="dealerDetails")
	public void dealVal(String assetType, String assetCategory, String make, String dealer, String source, String DSE) throws InterruptedException {
		
		SelectDealerPage sdp=new SelectDealerPage(driver);
		SwipeOperation sp=new SwipeOperation(driver);

		//select asset type
		erc=new ExtentReportsClass(driver);
		try {
			sdp.getAssetTypeDrpdwn().click();		
			sp.selectValueInList(assetType);
			log.info("Asset type: "+assetType);

			//select asset category
			sdp.getAssetCategoryDrpdwn().click();
			sp.selectValueInList(assetCategory);
			log.info("Asset category: "+assetCategory);

			//select make
			sdp.getMakeDrpdwn().click();
			Thread.sleep(2000);
			sp.selectValueInList(make);
			log.info("Make: "+make);

			//select dealer
			sdp.getDealerDrpdwn().click();
			sp.selectValueInList(dealer);
			log.info("Dealer: "+dealer);

			//select source
			sdp.getSourceDrpdwn().click();
			sp.selectValueInList(source);
			log.info("Source: "+source);

			//select DSE

			if (sdp.getDSEDrpdwn().isEnabled()) {
				sdp.getDSEDrpdwn().click();
				sp.selectValueInList(DSE);
				log.info("DSE: "+DSE);
			}
			else {
				log.info("DSE field is disabled");
			}
			//select radio button
			sdp.getCustRadioBtn_No().click(); 

			sp.selectValueInList("NEXT");
			Thread.sleep(2000);
		}

		catch(Exception e) {
			e.printStackTrace();
			log.info("Exception caught "+e);
		}

		finally {
			log.info("Select Dealer verified");
		}

	}
	
	@Test(dataProviderClass=ConfigDataProvider.class, dataProvider="assetData",dependsOnMethods= {"dealVal"})
	public void assetVal(String assetModCat, String assetMod1, String assetMod2, String rtoLoc, String repayFreq, String custProfile, String scheme, String busPromo, String creditPromo, String otherPromo, String tenure, String invoiceAmount, String loanAmount) throws InterruptedException {
		
		AssetDetailsPage adp=new AssetDetailsPage(driver);
		SwipeOperation sp=new SwipeOperation(driver);
		
		adp.getAssetModelCategory().click();
		sp.selectValueInList(assetModCat);
		log.info("Asset model category: "+assetModCat);

		adp.getAssetModel1().click(); 
		sp.selectValueInList(assetMod1);
		log.info("Asset model1: "+assetMod1);
		
		adp.getAssetModel2().click();
		sp.selectValueInList(assetMod2);
		log.info("Asset Model2: "+assetMod2);

		//adp.getAssetDeliveredRadioBtn_no().click();

		adp.getRtoLocation().click(); 
		sp.selectValueInList(rtoLoc);
		log.info("RTO location: "+rtoLoc);

		adp.getRepaymentFrequency().click(); 
		sp.selectValueInList(repayFreq);
		log.info("Repayment frequency: "+repayFreq);

		//adp.getCustomerProfile().click(); 
		//sp.selectValueInList(custProfile);

		//scroll from Customer profile to scheme 
		adp.scroll("Scheme *");

		adp.getSchemeEle().click(); 
		sp.selectValueInList(scheme);
		log.info("Scheme: "+scheme);

		adp.getBusinessPromotion().click(); 
		sp.selectValueInList(busPromo);
		log.info("Business promotion: "+busPromo);

		adp.getCreditPromotion().click();
		sp.selectValueInList(creditPromo);
		log.info("Credit promotion: "+creditPromo);

		adp.getOtherPromotion().click();
		sp.selectValueInList(otherPromo);
		log.info("Other promotion: "+otherPromo);
		
		//adp.scroll("");

		adp.getTenureEle().sendKeys(tenure);
		log.info("Tenure: "+tenure);

		adp.getInvoiceAmount().sendKeys(invoiceAmount);
		log.info("Invoice amount: "+invoiceAmount);

		adp.getLoanAmount().sendKeys(loanAmount);
		log.info("Loan amount: "+loanAmount);

		adp.getSubmitBtn().click(); 
		Thread.sleep(2000);
		log.info("Asset details page verified");
	}
	
	
}
