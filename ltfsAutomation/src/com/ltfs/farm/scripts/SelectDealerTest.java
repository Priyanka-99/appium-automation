package com.ltfs.farm.scripts;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.ltfs.farm.pageobjects.AssetDetailsPage;
import com.ltfs.farm.pageobjects.SelectDealerPage;
import com.ltfs.generics.BaseLib;
import com.ltfs.generics.ConfigDataProvider;
import com.ltfs.generics.ExcelLib;
import com.ltfs.generics.ExtentReportsClass;
import com.ltfs.generics.SwipeOperation;

public class SelectDealerTest extends BaseLib{

	Logger log = Logger.getLogger(SelectDealerTest.class);
	ExtentReportsClass erc;
	SelectDealerPage sdp;
	ExcelLib elib;
	AssetDetailsPage adp;
	SwipeOperation sp;

	@Test(dataProviderClass=ConfigDataProvider.class, dataProvider="dealerDetails")
	public void testDealer(String assetType, String assetCategory, String make, String dealer, String source, String DSE, String existingCust, String loanId) throws InterruptedException {

		//select asset type
		try {
			erc=new ExtentReportsClass(driver);
			sdp=new SelectDealerPage(driver);
			sdp.elementVisible();
			sdp.dealerSelection(assetType, assetCategory, make, dealer, source, DSE, existingCust, loanId);
			log.info("Select Dealer Page verified");
			erc.Report("SelectDealer","Verifying dealer page", "PASS"); 
		}
		catch(Exception e) {
			log.info("Select dealer script failed "+e);
			erc.Report("SelectDealer", "Verifying dealer page", "FAIL", true);
		}
	}

	@Test 
	public void testNewDealer() throws InterruptedException {

		try {
			erc=new ExtentReportsClass(driver); 
			elib=new ExcelLib("C:\\Users\\user07\\eclipse-workspace\\ltfsAutomation\\src\\resources\\testData_Farm.xlsx"); 
			String assetType=elib.readData("selectdealer",1, 0); 
			String assetCategory=elib.readData("selectdealer",1, 1); 
			String make=elib.readData("selectdealer",1, 2); 
			String dealer=elib.readData("selectdealer",1, 3); 
			String source=elib.readData("selectdealer",1, 4); 
			String DSE=elib.readData("selectdealer",1, 5); 
			String existingCust=elib.readData("selectdealer",1, 6); 
			String loanNo=elib.readData("selectdealer",1, 7);

			sdp=new SelectDealerPage(driver);

			sdp.dealerSelection(assetType, assetCategory, make, dealer, source, DSE, existingCust, loanNo); 
			Thread.sleep(1000);

			//created object of AssetDetailsPage and calling getters and fetching data from excel sheet 
			adp=new AssetDetailsPage(driver);
			adp.elementPresent();

			String assetModCat=elib.readData("assetDetails",1, 0); 
			String assetMod1=elib.readData("assetDetails",1, 1); 
			String assetMod2=elib.readData("assetDetails",1, 2); 
			String asDelRadioBtn=elib.readData("assetDetails",1, 3); 
			String rtoLoc=elib.readData("assetDetails",1, 4); 
			String repayFreq=elib.readData("assetDetails",1, 5); 
			String custProfile=elib.readData("assetDetails",1, 6); 
			String scheme=elib.readData("assetDetails",1, 7); 
			String busPromo=elib.readData("assetDetails",1, 8); 
			String creditPromo=elib.readData("assetDetails",1, 9); 
			String otherPromo=elib.readData("assetDetails",1, 10); 
			String tenure=elib.readData("assetDetails",1, 11); 
			String invoiceAmount=elib.readData("assetDetails",1, 12);
			String loanAmount=elib.readData("assetDetails",1, 13);
			String loanDocs=elib.readData("assetDetails",2, 19);

			//erc=new ExtentReportsClass(driver); 
			adp.elementPresent();
			adp.assetValues(assetModCat, assetMod1, assetMod2, asDelRadioBtn, rtoLoc,repayFreq, custProfile, scheme, busPromo, creditPromo, otherPromo,
					tenure, invoiceAmount, loanAmount, loanDocs); 
			Thread.sleep(1000);
			log.info("New Dealer verified"); 
			erc.Report("Select Dealer","Verifying new dealer", "PASS"); 
		}
		catch(Exception e) {
			log.info(e);
			erc.Report("Select Dealer","Verifying new dealer", "FAIL", true);
		}
	}


	@Test
	public void testUsedDealer() {
		try {
			//extent report 
			erc=new ExtentReportsClass(driver);
			sp=new SwipeOperation(driver);
			elib=new ExcelLib( "C:\\Users\\user07\\eclipse-workspace\\ltfsAutomation\\src\\resources\\testData_Farm.xlsx"); 
			String assetType=elib.readData("selectdealer", 2, 0); 
			String assetCategory=elib.readData("selectdealer", 2, 1); 
			String make=elib.readData("selectdealer", 2, 2); 
			String dealer=elib.readData("selectdealer", 2, 3); 
			String source=elib.readData("selectdealer", 2, 4); 
			String DSE=elib.readData("selectdealer", 2, 5);
			String radioBtn=elib.readData("selectdealer", 2, 6);
			String loanNo=elib.readData("selectdealer", 2, 7);

			sdp=new SelectDealerPage(driver); 
			sdp.elementVisible();
			sdp.dealerSelection(assetType, assetCategory, make, dealer, source, DSE, radioBtn, loanNo);

			//created pbject of AssetDetailsPage and calling getters and fetching data from excel sheet 
			adp=new AssetDetailsPage(driver);
			adp.elementPresent();

			//String assetModCat=elib.readData("assetDetails",1, 0); 
			String assetMod1=elib.readData("assetDetails",2, 1); 
			String deliveryDate=elib.readData("assetDetails",2, 14); 
			String hmr=elib.readData("assetDetails",2, 15); 
			String regNo=elib.readData("assetDetails",2, 16); 
			String regDate=elib.readData("assetDetails",2, 17); 
			String manufactureDate=elib.readData("assetDetails",2, 18); 
			String rtoLoc=elib.readData("assetDetails",2, 4); 
			String repayFreq=elib.readData("assetDetails",2, 5); 
			//String custProfile=elib.readData("assetDetails",2, 6); 
			String scheme=elib.readData("assetDetails",2, 7); 
			String busPromo=elib.readData("assetDetails",2, 8); 
			String creditPromo=elib.readData("assetDetails",2, 9); 
			//String otherPromo=elib.readData("assetDetails",2, 10); 
			String tenure=elib.readData("assetDetails",2, 11); 
			String invoiceAmount=elib.readData("assetDetails",1, 12);
			String loanAmount=elib.readData("assetDetails",2, 13);
			String loanDocs=elib.readData("assetDetails",2, 19);

			adp.elementPresent();
			adp.getAssetModel1().click();
			sp.selectValueInList(assetMod1);
			log.info("Asset Model: "+assetMod1);
			adp.getAssetDeliveredRadioBtn_yes().click();
			adp.getDeliveryDateTxtBx().sendKeys(deliveryDate);
			log.info("Date of delivery: "+deliveryDate);
			adp.getHmrTxtBx().sendKeys(hmr);
			log.info("HMR: "+hmr);
			adp.getRegNoTxtBx().sendKeys(regNo);
			log.info("Registration no: "+regNo);
			adp.getRegDateTxtBx().sendKeys(regDate);
			log.info("Registration date: "+regDate);
			//adp.getAssetHypo_no().click();
			adp.getAssetHypoRadioBtn_yes().click();
			adp.getLoanClosedRadioBtn_yes().click();
			adp.getDocsHypo().click();
			sp.selectValueInList(loanDocs);
			log.info("Documents: "+loanDocs);
			adp.scroll("RTO Location");
			adp.getManufacturingDateTxtBx().sendKeys(manufactureDate);
			log.info("Manufacturing date: "+manufactureDate);
			adp.getRtoLocation().click();
			sp.selectValueInList(rtoLoc);
			log.info("RTO location: "+rtoLoc);
			adp.getRepaymentFrequency().click();
			sp.selectValueInList(repayFreq);
			log.info("Repayment frequency: "+repayFreq);
			adp.getSchemeEle().click();
			sp.selectValueInList(scheme);
			log.info("Scheme: "+scheme);
			adp.getBusinessPromotion().click();
			sp.selectValueInList(busPromo);
			log.info("Business promotion: "+busPromo);
			adp.getCreditPromotion().click();
			sp.selectValueInList(creditPromo);
			log.info("Credit promotion: "+creditPromo);
			//scroll 
			adp.scroll("SUBMIT");
			adp.getTenureEle().sendKeys(tenure);
			log.info("Tenure: "+tenure);
			adp.getInvoiceAmount().sendKeys(invoiceAmount);
			log.info("Tractor valuation amount: "+invoiceAmount);
			adp.getLoanAmount().sendKeys(loanAmount);
			log.info("Loan amount: "+loanAmount);
			adp.getSubmitBtn().click();

			log.info("Used dealer verified");
			erc.Report("Select Dealer","Verifying used dealer", "PASS"); 
		}
		catch(Exception e) {
			log.info(e);
			erc.Report("Select Dealer","Verifying used dealer", "FAIL", true); 
		}
	}

	@Test
	public void testPreOwnedType() {
		try {
			//extent report 
			erc=new ExtentReportsClass(driver);
			sp=new SwipeOperation(driver);
			elib=new ExcelLib( "C:\\Users\\user07\\eclipse-workspace\\ltfsAutomation\\src\\resources\\testData_Farm.xlsx"); 
			String assetType=elib.readData("selectdealer", 3, 0); 
			String assetCategory=elib.readData("selectdealer", 3, 1); 
			String make=elib.readData("selectdealer", 3, 2); 
			String dealer=elib.readData("selectdealer", 3, 3); 
			String source=elib.readData("selectdealer", 3, 4); 
			String DSE=elib.readData("selectdealer", 3, 5);
			String radioBtn=elib.readData("selectdealer", 3, 6);
			String loanNo=elib.readData("selectdealer", 3, 7);

			sdp=new SelectDealerPage(driver); 
			//sdp.elementVisible();
			sdp.dealerSelection(assetType, assetCategory, make, dealer, source, DSE, radioBtn, loanNo);

			//created pbject of AssetDetailsPage and calling getters and fetching data from excel sheet 
			adp=new AssetDetailsPage(driver);
			adp.elementPresent();

			String assetMod1=elib.readData("assetDetails",3, 1); 
			String deliveryDate=elib.readData("assetDetails",3, 14); 
			String hmr=elib.readData("assetDetails", 3, 15); 
			String regNo=elib.readData("assetDetails", 3, 16); 
			String regDate=elib.readData("assetDetails", 3, 17); 
			String manufactureDate=elib.readData("assetDetails", 3, 18); 
			String rtoLoc=elib.readData("assetDetails", 3, 4); 
			String repayFreq=elib.readData("assetDetails", 3, 5); 
			//String custProfile=elib.readData("assetDetails", 3 , 6); 
			String scheme=elib.readData("assetDetails", 3, 7); 
			String busPromo=elib.readData("assetDetails", 3, 8); 
			String creditPromo=elib.readData("assetDetails", 3, 9); 
			//String otherPromo=elib.readData("assetDetails",2, 10); 
			String tenure=elib.readData("assetDetails", 3, 11); 
			String invoiceAmount=elib.readData("assetDetails", 3, 12);
			String loanAmount=elib.readData("assetDetails", 3, 13);
			//String loanDocs=elib.readData("assetDetails", 3, 19);
			String loanPurpose=elib.readData("assetDetails", 3, 20);

			adp.elementPresent();
			adp.getAssetModel1().click();
			sp.selectValueInList(assetMod1);
			log.info("Asset Model: "+assetMod1);
			adp.getAssetDeliveredRadioBtn_yes().click();
			adp.getDeliveryDateTxtBx().sendKeys(deliveryDate);
			log.info("Date of delivery: "+deliveryDate);
			adp.getHmrTxtBx().sendKeys(hmr);
			log.info("HMR: "+hmr);
			adp.getRegNoTxtBx().sendKeys(regNo);
			log.info("Registration no: "+regNo);
			adp.getRegDateTxtBx().sendKeys(regDate);
			log.info("Registration date: "+regDate);
			adp.getAssetFinRadioBtn_yes().click();
			adp.getAssetHypoRadioBtn_yes().click();
			adp.getLoanClosedRadioBtn_yes().click();
			//adp.getAssetFinRadioBtn_no().click();
			//adp.getAssetHypoRadioBtn_no().click();
			//adp.getDocsHypo().click();
			//sp.selectValueInList(loanDocs);
			//log.info("Documents: "+loanDocs);

			//scroll
			adp.scroll("Date of Manufacturing");
			adp.getManufacturingDateTxtBx().sendKeys(manufactureDate);
			log.info("Manufacturing date: "+manufactureDate);
			adp.getRtoLocation().click();
			sp.selectValueInList(rtoLoc);
			log.info("RTO location: "+rtoLoc);
			adp.getRepaymentFrequency().click();
			sp.selectValueInList(repayFreq);
			log.info("Repayment frequency: "+repayFreq);
			adp.getLoanPurpose().click();
			sp.selectValueInList(loanPurpose);
			log.info("Purpose of loan: "+loanPurpose);
			adp.scroll("SUBMIT");
			adp.getSchemeEle().click();
			sp.selectValueInList(scheme);
			log.info("Scheme: "+scheme);
			adp.getBusinessPromotion().click();
			sp.selectValueInList(busPromo);
			log.info("Business promotion: "+busPromo);
			adp.getCreditPromotion().click();
			sp.selectValueInList(creditPromo);
			log.info("Credit promotion: "+creditPromo);
			adp.getTenureEle().sendKeys(tenure);
			log.info("Tenure: "+tenure);
			adp.getInvoiceAmount().sendKeys(invoiceAmount);
			log.info("Tractor valuation amount: "+invoiceAmount);
			adp.getLoanAmount().sendKeys(loanAmount);
			log.info("Loan amount: "+loanAmount);
			adp.getSubmitBtn().click();
			
			log.info("Asset Details Page verified");
			erc.Report("Select Dealer","Verifying pre-owned dealer", "PASS"); 
		}
		catch(Exception e) {
			log.info(e);
			erc.Report("Select Dealer","Verifying pre-owned dealer", "FAIL", true); 
		}
	}

}
