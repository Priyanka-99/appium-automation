package com.ltfs.farm.scripts;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.ltfs.farm.pageobjects.DashboardPage;
import com.ltfs.farm.pageobjects.LoginPage;
import com.ltfs.generics.BaseLib;
import com.ltfs.generics.ConfigDataProvider;
import com.ltfs.generics.ExtentReportsClass;

import io.appium.java_client.MobileElement;

public class LoginTest extends BaseLib{

	Logger log = Logger.getLogger(LoginTest.class);
	ExtentReportsClass erc;

	/**
	 * LoginTest class will test the login page with valid and invalid username and password inputs
	 */

	@Test(dataProviderClass=ConfigDataProvider.class, dataProvider="readInvalidLogin") 
	public void invalidLogin(String username, String pswd) throws InterruptedException {

		//invalid login erc=new ExtentReportsClass(driver); 
		try {
		LoginPage lp=new LoginPage(driver); 
		lp.getUserName().sendKeys(username);
		lp.getPswd().sendKeys(pswd); 
		lp.getLoginBtn().click();
		log.info("Username: "+username+" Password: "+pswd); 
		Thread.sleep(2000);
		//driver.manage().timeouts().implicitlyWait(2, SECONDS);

		boolean flag=lp.getErrorMsg().isDisplayed(); 
		System.out.println(flag);
		if(flag){ 
			Assert.assertTrue(true, "Invalid login");
			log.info("Invalid login is verified");
			erc.Report("Verifying invalid login","Invalid login", "PASS"); 
		}
		}catch(Exception e) {
			log.info(e);
			erc.Report("Verifying invalid login", "Invalid login", "FAIL", true);
		}
		
	}

	@Test(priority=1, dataProviderClass=ConfigDataProvider.class, dataProvider="readValidLogin")
	public void validLogin(String username, String pswd) throws InterruptedException {

		//valid login
		try {
		erc=new ExtentReportsClass(driver);
		LoginPage lp=new LoginPage(driver);
		lp.getUserName().sendKeys(username);
		lp.getPswd().sendKeys(pswd);
		lp.getLoginBtn().click();
		Thread.sleep(40000);
		log.info("Username: "+username+" Password: "+pswd);
		Assert.assertTrue(true, "Valid login");
		log.info("Valid login is verified");
		erc.Report("Verifying valid login", "Valid login", "PASS");
		}
		catch(Exception e) {
			log.info(e);
			erc.Report("Verifying valid login", "Valid login", "FAIL", true);
		}
	}

}

