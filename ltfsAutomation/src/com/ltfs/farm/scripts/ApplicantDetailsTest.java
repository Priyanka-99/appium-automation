package com.ltfs.farm.scripts;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import com.ltfs.farm.pageobjects.AadharCapturePage;
import com.ltfs.farm.pageobjects.AadharOCRPage;
import com.ltfs.farm.pageobjects.AadharPromptPage;
import com.ltfs.farm.pageobjects.ApplicantDetailsPage;
import com.ltfs.farm.pageobjects.ManualAadharPage;
import com.ltfs.generics.BaseLib;
import com.ltfs.generics.ConfigDataProvider;
import com.ltfs.generics.ExcelLib;
import com.ltfs.generics.ExtentReportsClass;

public class ApplicantDetailsTest extends BaseLib {

	Logger log = Logger.getLogger(ApplicantDetailsTest.class);
	ExtentReportsClass erc;
	ApplicantDetailsPage adp;
	AadharPromptPage ap;
	AadharOCRPage aop;
	AadharCapturePage acp;

	@Test(dataProviderClass=ConfigDataProvider.class, dataProvider="applicantInputs")
	public void testApplicantDetails(String mobile, String emailId) throws InterruptedException {
		
		try {
		erc=new ExtentReportsClass(driver);
		//Navigating to Applicant Details Page
		adp = new ApplicantDetailsPage(driver);
		Thread.sleep(1000);
		adp.applicantValues(mobile, emailId);
		log.info("Mobile no: "+mobile);
		log.info("Email id: "+emailId);
		log.info("Applicant Details Page verified");
		erc.Report("Applicant Details", "Verifying applicant details", "FAIL");

		//Navigating KYC page
		ap=new AadharPromptPage(driver);
		ap.elementVisible(ap.getYesBtn());
		ap.getYesBtn().click();
		Thread.sleep(3000);
		log.info("Aadhar Prompt verified"); 
		erc.Report("Aadhar prompt", "Verifying aadhar prompt(KYC)", "FAIL");

		//Navigating AadharOCR page 
		aop=new AadharOCRPage(driver);
		aop.elementVisible(aop.getOcrEle());
		aop.getOcrEle().click();
		Thread.sleep(2000);

		for(int i=1;i<=3;i++) {
			aop.takePhoto(driver);
			System.out.println("OCR Skipped "+i+" time(s)");
		}
		Thread.sleep(1000);
		log.info("Aadhar OCR page verified");
		erc.Report("Aadhar OCR", "Verifying Aadhar OCR", "FAIL");
		
		//Navigating to Aadhar Capture page
		acp=new AadharCapturePage(driver);
		for(int i=1;i<=3;i++) {
			acp.skipAadharCapture(driver);
			System.out.println("QR Skipped "+i+" time(s)");
		}
		log.info("Aadhar capture page verified");
		erc.Report("Aadhar capture", "Verifying aadhar QR capture", "FAIL");
		
		//Navigating to manual aadhar details page
		ManualAadharPage mp=new ManualAadharPage(driver);
		mp.elementVisible(mp.getAadharEle());
		
		ExcelLib elib=new ExcelLib("C:\\Users\\user07\\eclipse-workspace\\ltfsAutomation\\src\\resources\\testData_Farm.xlsx"); 
		String aadharNo=elib.readData("manualAadhar", 1, 0);
		String firstName=elib.readData("manualAadhar", 1, 1);
		String lastName=elib.readData("manualAadhar", 1, 2);
		String fatherName=elib.readData("manualAadhar", 1, 3);
		String dob=elib.readData("manualAadhar", 1, 4);
		String motherName=elib.readData("manualAadhar", 1, 5);
		String alternateMobNum=elib.readData("manualAadhar", 1, 6);
		String gender=elib.readData("manualAadhar", 1, 7);
		String add1=elib.readData("manualAadhar", 1, 8);
		String add2=elib.readData("manualAadhar", 1, 9);
		String pincode=elib.readData("manualAadhar", 1, 10);
		String maritalStatus=elib.readData("manualAadhar", 1, 11);
		String occupation=elib.readData("manualAadhar", 1, 12);
		
		mp.aadharDetails(aadharNo, firstName, lastName, fatherName, dob, motherName, alternateMobNum, gender, add1, add2, pincode, maritalStatus, occupation);
		Thread.sleep(2000);
		log.info("Manual Aadhar details page verified");
		erc.Report("Manual Aadhar details", "Verifying manual aadhar details", "FAIL");
		}
		catch(Exception e) {
			log.info(e);
		}
	}

}