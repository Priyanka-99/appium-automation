package com.ltfs.farm.scripts;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import com.ltfs.farm.pageobjects.DashboardPage;
import com.ltfs.generics.BaseLib;
import com.ltfs.generics.ConfigDataProvider;
import com.ltfs.generics.SwipeOperation;

public class DashboardTest extends BaseLib{
	Logger log = Logger.getLogger(DashboardTest.class);

	/*	@Test
	public void testDashboard() throws InterruptedException {
		//tap on new
		DashboardPage dp=new DashboardPage(driver);
		dp.getNewEle().click();
		Thread.sleep(3000);
		log.info("Dashboard verified");	
	} */

	@Test(dataProviderClass=ConfigDataProvider.class, dataProvider="dashboardInputs")
	public void testSearchIcon(String param, String val) throws InterruptedException {
		try {
			DashboardPage dp=new DashboardPage(driver);
			SwipeOperation sp=new SwipeOperation(driver);
			dp.elementVisible(dp.getSearchEle());
			dp.getSearchEle().click();
			dp.elementVisible(dp.getChooseSearchEle());
			dp.getChooseSearchEle().click();
			sp.selectValueInList(param);
			dp.elementVisible(dp.getSearchByNameEle());
			dp.getSearchByNameEle().sendKeys(val);
			log.info("Name: "+val);
			log.info("Dashboard search verified");
		}
		catch(Exception e) {
			log.info(e);
		}
	}

	/*
	 * @Test(priority=2) public void tc1() throws InterruptedException { //tap on
	 * dashboard icon DashboardPage dp=new DashboardPage(driver);
	 * dp.getDashboardEle().click(); Thread.sleep(3000); }
	 * 
	 * @Test(priority=3) public void tc2() throws InterruptedException {
	 * DashboardPage dp=new DashboardPage(driver); //tap on logout icon
	 * dp.getLogoutEle().click(); //tap on yes button dp.getAcceptBtn().click();
	 * Thread.sleep(3000); }
	 * 
	 * 
	 * @Test(priority=4) public void tc4() throws InterruptedException {
	 * DashboardPage dp=new DashboardPage(driver); dp.getRefEle().click(); }
	 * 
	 * 
	 * @Test(priority=5) public void tc5() throws InterruptedException {
	 * DashboardPage dp=new DashboardPage(driver); dp.getProfileEle().click(); }
	 * 
	 * 
	 * 
	 * @Test(priority=6) public void tc6() throws InterruptedException {
	 * DashboardPage dp=new DashboardPage(driver); dp.getSearchEle().click(); }
	 */
}
