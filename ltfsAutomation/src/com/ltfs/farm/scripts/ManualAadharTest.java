package com.ltfs.farm.scripts;

import org.apache.log4j.Logger;
import org.testng.annotations.Test;
import com.ltfs.farm.pageobjects.ManualAadharPage;
import com.ltfs.generics.BaseLib;
import com.ltfs.generics.ConfigDataProvider;
import com.ltfs.generics.ExtentReportsClass;
import com.ltfs.generics.SwipeOperation;

public class ManualAadharTest extends BaseLib{
	
	Logger log = Logger.getLogger(ManualAadharTest.class);
	ExtentReportsClass erc;

	@Test(dataProviderClass=ConfigDataProvider.class, dataProvider="manualAadharDetails")
	public void aadharDetails(String aadharNo, String firstName, String lastName, String fatherName, String dob, 
			String motherName, String alternateMobNum, String gender, String add1, String add2, String pincode, String maritalStatus, 
			String occupation) throws InterruptedException {
		try {
		erc=new ExtentReportsClass(driver);
		ManualAadharPage mp=new ManualAadharPage(driver);
		mp.aadharDetails(aadharNo, firstName, lastName, fatherName, dob, motherName, alternateMobNum, gender, add1, 
				add2, pincode, maritalStatus, occupation);
		log.info("Manual Aadhar details page verified");
		erc.Report("Manual Aadhar details", "Verifying manual aadhar details", "PASS");
		}
		catch(Exception e) {
			log.info(e);
			erc.Report("Manual Aadhar details", "Verifying manual aadhar details", "FAIL", true);
		}
	}

}
