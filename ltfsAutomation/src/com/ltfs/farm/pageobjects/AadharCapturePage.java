package com.ltfs.farm.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AadharCapturePage {

	Logger log = Logger.getLogger(AadharCapturePage.class);
	AndroidDriver<MobileElement> driver;

	public AadharCapturePage(AndroidDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/scanButton")
	private MobileElement scanBtn;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/aadharnumber")
	private MobileElement aadharNoTxtBx;
	
	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/skip_button")
	private MobileElement skipBtn;

	//getters
	public MobileElement getScanBtn() {
		return scanBtn;
	}

	public MobileElement getAadharNoTxtBx() {
		return aadharNoTxtBx;
	}

	public MobileElement getSkipBtn() {
		return skipBtn;
	}
	
	
	public void elementVisible(MobileElement element) {
		WebDriverWait wait=new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public void skipAadharCapture(AndroidDriver<MobileElement> driver) {
		elementVisible(scanBtn);
		scanBtn.click();
		elementVisible(skipBtn);
		skipBtn.click();
	}

}
