package com.ltfs.farm.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class DashboardPage{
	
	AndroidDriver<MobileElement> driver;
	Logger log = Logger.getLogger(DashboardPage.class);

	public DashboardPage(AndroidDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/bb_bottom_bar_icon")
	private MobileElement dashboardEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/item1")
	private MobileElement logoutEle;

	@AndroidFindBy(xpath="android:id/button1")
	private MobileElement acceptBtn;

	@AndroidFindBy(xpath="android:id/button2")
	private MobileElement rejectBtn;

	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[4]/android.widget.ImageView")
	private MobileElement newEle;

	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[2]/android.widget.TextView")
	private MobileElement refEle;

	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[5]/android.widget.TextView")
	private MobileElement profileEle;

	@AndroidFindBy(xpath="/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.LinearLayout[2]/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.LinearLayout[1]/android.widget.TextView")
	private MobileElement searchEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner")
	private MobileElement chooseSearchEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/applicantname")
	private MobileElement searchByNameEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/fromdate")
	private MobileElement searchByDate;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinnerStatus")
	private MobileElement searchByStatus;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/applicantmobile")
	private MobileElement searchByMobile;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/applicantGroupId")
	private MobileElement searchByAppID;

	//getters
	public MobileElement getDashboardEle() {
		return dashboardEle;
	}

	public MobileElement getLogoutEle() {
		return logoutEle;
	}

	public MobileElement getAcceptBtn() {
		return acceptBtn;
	}

	public MobileElement getNewEle() {
		return newEle;
	}

	public MobileElement getRefEle() {
		return refEle;
	}

	public MobileElement getProfileEle() {
		return profileEle;
	}

	public MobileElement getSearchEle() {
		return searchEle;
	}

	public MobileElement getSearchByNameEle() {
		return searchByNameEle;
	}

	public MobileElement getSearchByDate() {
		return searchByDate;
	}

	public MobileElement getSearchByStatus() {
		return searchByStatus;
	}

	public MobileElement getSearchByMobile() {
		return searchByMobile;
	}

	public MobileElement getSearchByAppID() {
		return searchByAppID;
	}

	public MobileElement getChooseSearchEle() {
		return chooseSearchEle;
	}

	//webdriver wait 
	public void elementVisible(MobileElement element) {
		WebDriverWait wait=new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));	
	}

}
