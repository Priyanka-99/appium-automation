package com.ltfs.farm.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AadharOCRPage {

	Logger log = Logger.getLogger(AadharOCRPage.class);
	AndroidDriver<MobileElement> driver;

	public AadharOCRPage(AndroidDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/aadharnumber_ocr")
	private MobileElement aadharOcrTxtBx;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/capture_ocr")
	private MobileElement ocrEle;
	
	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/cb_aadhaar_front")
	private MobileElement aadharFrontChkbx;
	
	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/cb_aadhaar_back")
	private MobileElement aadharBackChkbx;
	
	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/btn_proceed")
	private MobileElement proceedBtn;
	
	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/camera_bubble")
	private MobileElement cameraEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/confirm_button")
	private MobileElement usePhotoBtn;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/retake_button")
	private MobileElement retakeBtn;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/retake_button")
	private MobileElement crossbtn;

	//getters
	public MobileElement getOcrEle() {
		return ocrEle;
	}

	public MobileElement getCameraEle() {
		return cameraEle;
	}

	public MobileElement getUsePhotoBtn() {
		return usePhotoBtn;
	}

	public MobileElement getAadharOcrTxtBx() {
		return aadharOcrTxtBx;
	}

	public MobileElement getAadharFrontChkbx() {
		return aadharFrontChkbx;
	}

	public MobileElement getAadharBackChkbx() {
		return aadharBackChkbx;
	}

	public MobileElement getProceedBtn() {
		return proceedBtn;
	}

	public MobileElement getRetakeBtn() {
		return retakeBtn;
	}

	public MobileElement getCrossbtn() {
		return crossbtn;
	}	

	public void elementVisible(MobileElement element) {
		WebDriverWait wait=new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
	
	public void takePhoto(AndroidDriver<MobileElement> driver) {
		elementVisible(aadharFrontChkbx);
		aadharFrontChkbx.click();
		//aop.getProceedBtn().click();
		elementVisible(cameraEle);
		cameraEle.click();
		elementVisible(usePhotoBtn);
		usePhotoBtn.click();
	}

}
