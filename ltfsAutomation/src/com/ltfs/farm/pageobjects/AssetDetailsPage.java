package com.ltfs.farm.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.ltfs.generics.BaseLib;
import com.ltfs.generics.SwipeOperation;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class AssetDetailsPage extends BaseLib{

	Logger log = Logger.getLogger(AssetDetailsPage.class);

	public AssetDetailsPage(AndroidDriver<MobileElement> driver) {
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_asset_model_category")
	private MobileElement assetModelCategory;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_asset_model")
	private MobileElement assetModel1;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spHp")
	private MobileElement assetModel2;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radion_asset_delivered_yes")
	private MobileElement assetDeliveredRadioBtn_yes;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radion_asset_delivered_no")
	private MobileElement assetDeliveredRadioBtn_no;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/datepurchase")
	private MobileElement deliveryDateTxtBx;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/auto_text_view_tractorKms")
	private MobileElement hmrTxtBx;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/ed_reg_number")
	private MobileElement regNoTxtBx;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/ed_reg_date")
	private MobileElement regDateTxtBx;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_group_hypo_no")
	private MobileElement assetHypoRadioBtn_no;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_group_hypo_yes")
	private MobileElement assetHypoRadioBtn_yes;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_group_loanClosed_no")
	private MobileElement loanClosedRadioBtn_no;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_group_loanClosed_yes")
	private MobileElement loanClosedRadioBtn_yes;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_docs_hypo")
	private MobileElement docsHypo;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_group_asset_yes")
	private MobileElement assetFinRadioBtn_yes;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_group_asset_no")
	private MobileElement assetFinRadioBtn_no;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_group_asset2years_yes")
	private MobileElement assetLoanRadioBtn_yes;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_group_asset2years_no")
	private MobileElement assetLoanRadioBtn_no;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/ed_manufacturing_date")
	private MobileElement manufacturingDateTxtBx;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_rto_location")
	private MobileElement rtoLocation;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_repayment_frequency")
	private MobileElement repaymentFrequency;
	
	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_use_of_funds")
	private MobileElement loanPurpose;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_customer_profile")
	private MobileElement customerProfile;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_scheme")
	private MobileElement schemeEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_promo")
	private MobileElement businessPromotion;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_credit_promo")
	private MobileElement creditPromotion;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_Otherpromo")
	private MobileElement otherPromotion;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/auto_text_view_tenure")
	private MobileElement tenureEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/auto_text_view_invoice")
	private MobileElement invoiceAmount;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/auto_text_view_loan_amount_value")
	private MobileElement loanAmount;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/ed_margin_money")
	private MobileElement marginMoney;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/button_submit")
	private MobileElement submitBtn;

	//getters
	public MobileElement getAssetModelCategory() {
		return assetModelCategory;
	}

	public MobileElement getAssetModel1() {
		return assetModel1;
	}

	public MobileElement getAssetModel2() {
		return assetModel2;
	}

	public MobileElement getAssetDeliveredRadioBtn_yes() {
		return assetDeliveredRadioBtn_yes;
	}

	public MobileElement getAssetDeliveredRadioBtn_no() {
		return assetDeliveredRadioBtn_no;
	}

	public MobileElement getDeliveryDateTxtBx() {
		return deliveryDateTxtBx;
	}

	public MobileElement getHmrTxtBx() {
		return hmrTxtBx;
	}

	public MobileElement getRegNoTxtBx() {
		return regNoTxtBx;
	}

	public MobileElement getRegDateTxtBx() {
		return regDateTxtBx;
	}

	public MobileElement getAssetHypoRadioBtn_no() {
		return assetHypoRadioBtn_no;
	}

	public MobileElement getAssetHypoRadioBtn_yes() {
		return assetHypoRadioBtn_yes;
	}

	public MobileElement getLoanClosedRadioBtn_no() {
		return loanClosedRadioBtn_no;
	}

	public MobileElement getLoanClosedRadioBtn_yes() {
		return loanClosedRadioBtn_yes;
	}

	public MobileElement getDocsHypo() {
		return docsHypo;
	}

	public MobileElement getAssetFinRadioBtn_yes() {
		return assetFinRadioBtn_yes;
	}

	public MobileElement getAssetFinRadioBtn_no() {
		return assetFinRadioBtn_no;
	}

	public MobileElement getAssetLoanRadioBtn_yes() {
		return assetLoanRadioBtn_yes;
	}

	public MobileElement getAssetLoanRadioBtn_no() {
		return assetLoanRadioBtn_no;
	}

	public MobileElement getManufacturingDateTxtBx() {
		return manufacturingDateTxtBx;
	}

	public MobileElement getRtoLocation() {
		return rtoLocation;
	}

	public MobileElement getRepaymentFrequency() {
		return repaymentFrequency;
	}
	
	public MobileElement getLoanPurpose() {
		return loanPurpose;
	}

	public MobileElement getCustomerProfile() {
		return customerProfile;
	}

	public MobileElement getSchemeEle() {
		return schemeEle;
	}

	public MobileElement getBusinessPromotion() {
		return businessPromotion;
	}

	public MobileElement getCreditPromotion() {
		return creditPromotion;
	}

	public MobileElement getOtherPromotion() {
		return otherPromotion;
	}

	public MobileElement getTenureEle() {
		return tenureEle;
	}

	public MobileElement getInvoiceAmount() {
		return invoiceAmount;
	}

	public MobileElement getLoanAmount() {
		return loanAmount;
	}

	public MobileElement getMarginMoney() {
		return marginMoney;
	}

	public MobileElement getSubmitBtn() {
		return submitBtn;
	}

	public void elementPresent() {
		WebDriverWait wait = new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(assetModel1));
	}

	//reusable scroll method
	public void scroll(String visibleText) {
		driver.findElement(
				MobileBy.AndroidUIAutomator(
						"new UiScrollable(new UiSelector()).scrollIntoView("
								+"new UiSelector().text(\""+visibleText+"\"));"));
	}

	public void assetValues(String assetModCat, String assetMod1, String assetMod2, String asDelRadioBtn, String rtoLoc, String repayFreq, String custProfile, 
			String scheme, String busPromo, String creditPromo, String otherPromo, String tenure, String invoiceAmt, String loanAmt, String loanDocs) {

		try{
			SwipeOperation sp=new SwipeOperation(driver);

			assetModelCategory.click();
			sp.selectValueInList(assetModCat);
			log.info("Asset model category: "+assetModCat);

			assetModel1.click(); 
			sp.selectValueInList(assetMod1);
			log.info("Asset model1: "+assetMod1);

			assetModel2.click(); 
			sp.selectValueInList(assetMod2);
			log.info("Asset Model2: "+assetMod2);

			sp.selectValueInList(asDelRadioBtn);

			rtoLocation.click(); 
			sp.selectValueInList(rtoLoc);
			log.info("RTO location: "+rtoLoc);

			repaymentFrequency.click(); 
			sp.selectValueInList(repayFreq);
			log.info("Repayment frequency: "+repayFreq);

			//adp.getCustomerProfile().click(); 
			//sp.selectValueInList(custProfile);

			//scroll from Customer profile to scheme 
			scroll("Scheme *");

			schemeEle.click(); 
			sp.selectValueInList(scheme);
			log.info("Scheme: "+scheme);

			businessPromotion.click(); 
			Thread.sleep(1000);
			sp.selectValueInList(busPromo);
			log.info("Business promotion: "+busPromo);

			creditPromotion.click(); 
			sp.selectValueInList(creditPromo);
			log.info("Credit promotion: "+creditPromo);

			otherPromotion.click(); 
			sp.selectValueInList(otherPromo);
			log.info("Other promotion: "+otherPromo);

			//adp.scroll(""); 
			tenureEle.sendKeys(tenure); 
			log.info("Tenure: "+tenure);

			invoiceAmount.sendKeys(invoiceAmt);
			log.info("Invoice amount: "+invoiceAmt);

			loanAmount.sendKeys(loanAmt);
			log.info("Loan amount: "+loanAmt);

			submitBtn.click(); 
		}
		catch(Exception e) {
			System.out.print("Exception caught");
			log.info("Exception caught");
		}
	}

}
