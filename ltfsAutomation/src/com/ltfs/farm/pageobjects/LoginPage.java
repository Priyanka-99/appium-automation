package com.ltfs.farm.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class LoginPage{

	Logger log = Logger.getLogger(LoginPage.class);
	AndroidDriver<MobileElement> driver;

	public LoginPage(AndroidDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/psnumber")
	private MobileElement userName; 

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/password")
	private MobileElement pswd;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/email_sign_in_button")
	private MobileElement loginBtn;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/errorAttemptMSG")
	private MobileElement errorMsg;


	//getters
	public MobileElement getUserName() {
		return userName;
	}

	public MobileElement getPswd() {
		return pswd;
	}

	public MobileElement getLoginBtn() {
		return loginBtn;
	}

	public MobileElement getErrorMsg() {
		return errorMsg;
	}


	//reusable method
	public void login(String username, String password) throws InterruptedException {
		userName.sendKeys(username);
		pswd.sendKeys(password);
		loginBtn.click();
	}
	
	//wait until web element is visible
	public void elementVisible(MobileElement element) {
		WebDriverWait wait=new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
	}
}
