package com.ltfs.farm.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.ltfs.generics.SwipeOperation;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ManualAadharPage {

	Logger log = Logger.getLogger(ManualAadharPage.class);
	AndroidDriver<MobileElement> driver;

	public ManualAadharPage(AndroidDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/act_aadhar")
	private MobileElement aadharEle; 

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_no")
	private MobileElement panCardRadioBtn;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/act_first_name")
	private MobileElement firstNameEle; 

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/act_last_name")
	private MobileElement lastNameEle; 

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/act_father_spouse_name")
	private MobileElement fatherNameEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/act_dob")
	private MobileElement dobEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/et_row_applicant_mothers_name")
	private MobileElement maidenEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/et_applicant_alternate_mobile")
	private MobileElement alternateMobileEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/radio_account_no")
	private MobileElement bankRadioBtn; 

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_gender")
	private MobileElement genderEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/act_mobile_number") 
	private MobileElement mobileEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/act_address_line_one") 
	private MobileElement addLine1;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/act_address_line_two") 
	private MobileElement addLine2;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/act_pincode") 
	private MobileElement pincodeEle;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_row_applicant_marital_status") 
	private MobileElement maritalStatusDrpdwn;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/spinner_row_applicant_occupation") 
	private MobileElement occupationDrpdwn;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/cb_address_same") 
	private MobileElement addressSameChkBx;

	//@AndroidFindBy(id="com.ltfs.lti.ltfs:id/cb_agree_consent") 
	//private MobileElement consentChkBx;

	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/btn_proceed")
	private MobileElement proceedBtn;

	//getters
	public MobileElement getAadharEle() {
		return aadharEle;
	}

	public MobileElement getPanCardRadioBtn() {
		return panCardRadioBtn;
	}

	public MobileElement getMaidenEle() {
		return maidenEle;
	}

	public MobileElement getAlternateMobileEle() {
		return alternateMobileEle;
	}

	public MobileElement getFirstNameEle() {
		return firstNameEle;
	}

	public MobileElement getLastNameEle() {
		return lastNameEle;
	}

	public MobileElement getFatherNameEle() {
		return fatherNameEle;
	}

	public MobileElement getDobEle() {
		return dobEle;
	}

	public MobileElement getBankRadioBtn() {
		return bankRadioBtn;
	}

	public MobileElement getGenderEle() {
		return genderEle;
	}

	public MobileElement getMobileEle() {
		return mobileEle;
	}

	public MobileElement getAddLine1() {
		return addLine1;
	}

	public MobileElement getAddLine2() {
		return addLine2;
	}

	public MobileElement getPincodeEle() {
		return pincodeEle;
	}

	public MobileElement getMaritalStatusDrpdwn() {
		return maritalStatusDrpdwn;
	}

	public MobileElement getOccupationDrpdwn() {
		return occupationDrpdwn;
	}

	public MobileElement getAddressSameChkBx() {
		return addressSameChkBx;
	}

	public MobileElement getProceedBtn() {
		return proceedBtn;
	}

	public void elementVisible(MobileElement element) {
		WebDriverWait wait=new WebDriverWait(driver, 20);
		wait.until(ExpectedConditions.visibilityOf(element));
	}

	//reusable scroll method
	public void scroll(String visibleText) {
		driver.findElement(
				MobileBy.AndroidUIAutomator(
						"new UiScrollable(new UiSelector()).scrollIntoView("
								+"new UiSelector().text(\""+visibleText+"\"));"));
	}

	public void aadharDetails(String aadharNo, String firstName, String lastName, String fatherName, String dob, String motherName, 
			String alternateMobNum, String gender, String add1, String add2, String pincode, String maritalStatus, String occupation) throws InterruptedException {
		SwipeOperation sp=new SwipeOperation(driver);
		aadharEle.sendKeys(aadharNo);
		log.info("Aadhar no: "+aadharNo);
		panCardRadioBtn.click();
		firstNameEle.sendKeys(firstName);
		log.info("First name: "+firstName);
		lastNameEle.sendKeys(lastName);
		log.info("Last name: "+lastName);
		fatherNameEle.sendKeys(fatherName);
		log.info("Father/spouse code: "+fatherName);
		dobEle.sendKeys(dob);
		log.info("DOB: "+dob);
		maidenEle.sendKeys(motherName);
		log.info("Mother/maiden name: "+motherName);
		alternateMobileEle.sendKeys(alternateMobNum);
		log.info("Alternative mobile no: "+alternateMobNum);
		bankRadioBtn.click();

		//scroll
		scroll("Gender*");
		genderEle.click();
		sp.selectValueInList(gender);

		addLine1.sendKeys(add1);
		log.info("Address line1: "+add1);
		addLine2.sendKeys(add2);
		log.info("Address line2: "+add2);
		pincodeEle.sendKeys(pincode);
		log.info("Pincode: "+pincode);

		//scroll
		scroll("Applicant Marital Status");
		maritalStatusDrpdwn.click();
		sp.selectValueInList(maritalStatus);
		occupationDrpdwn.click();
		sp.selectValueInList(occupation);
		addressSameChkBx.click();

		//scroll
		sp.selectValueInList("PROCEED");
		Thread.sleep(2000);
	}
}
