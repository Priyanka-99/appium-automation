package com.ltfs.farm.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class ApplicantDetailsPage{
	
Logger log = Logger.getLogger(ApplicantDetailsPage.class);
	
	AndroidDriver<MobileElement> driver;

	public ApplicantDetailsPage(AndroidDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	
	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/mobilenumber")
	private MobileElement mobileNoEle;
	
	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/emailId")
	private MobileElement emailEle;
	
	@AndroidFindBy(id="com.ltfs.lti.ltfs:id/btnsubmit")
	private MobileElement submitBtn;

	//getters
	public MobileElement getMobileNoEle() {
		return mobileNoEle;
	}

	public MobileElement getEmailEle() {
		return emailEle;
	}

	public MobileElement getSubmitBtn() {
		return submitBtn;
	}
	
	public void applicantValues(String mobile, String emailId) {
		mobileNoEle.sendKeys(mobile);
		emailEle.sendKeys(emailId);
		submitBtn.click();
	}
}
