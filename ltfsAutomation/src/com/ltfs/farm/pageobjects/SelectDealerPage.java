package com.ltfs.farm.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.ltfs.generics.SwipeOperation;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SelectDealerPage{

	Logger log = Logger.getLogger(SelectDealerPage.class);
	AndroidDriver<MobileElement> driver;

	public SelectDealerPage(AndroidDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	// declaring and storing mobile elements
	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/spinner_asset_type")
	private MobileElement assetTypeDrpdwn;

	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/spinner_asset_cat")
	private MobileElement assetCategoryDrpdwn;

	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/spinner_make")
	private MobileElement makeDrpdwn;

	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/sp_source")
	private MobileElement sourceDrpdwn;

	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/sp_dealer")
	private MobileElement dealerDrpdwn;

	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/spinner_dse")
	private MobileElement DSEDrpdwn;

	@AndroidFindBy(className = "//android.widget.RadioButton")
	private MobileElement custRadioBtn;

	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/radio_group_tmh_no")
	private MobileElement custRadioBtn_No;

	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/radio_group_tmh_yes")
	private MobileElement custRadioBtn_Yes;

	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/tv_existing_customer")
	private MobileElement existingCustIdEle;

	@AndroidFindBy(id = "com.ltfs.lti.ltfs:id/btnnext")
	private MobileElement nextBtn;


	//getters
	public MobileElement getAssetTypeDrpdwn() {
		return assetTypeDrpdwn;
	}

	public MobileElement getAssetCategoryDrpdwn() {
		return assetCategoryDrpdwn;
	}

	public MobileElement getMakeDrpdwn() {
		return makeDrpdwn;
	}

	public MobileElement getSourceDrpdwn() {
		return sourceDrpdwn;
	}

	public MobileElement getDealerDrpdwn() {
		return dealerDrpdwn;
	}

	public MobileElement getDSEDrpdwn() {
		return DSEDrpdwn;
	}

	public MobileElement getCustRadioBtn() {
		return custRadioBtn;
	}

	public MobileElement getCustRadioBtn_No() {
		return custRadioBtn_No;
	}

	public MobileElement getCustRadioBtn_Yes() {
		return custRadioBtn_Yes;
	}

	public MobileElement getExistingCustIdEle() {
		return existingCustIdEle;
	}

	public MobileElement getNextBtn() {
		return nextBtn;
	}

	public void elementVisible() {
		WebDriverWait wait=new WebDriverWait(driver,20);
		wait.until(ExpectedConditions.visibilityOf(assetCategoryDrpdwn));
	}

	public void scroll(String visibleText) {
		driver.findElement(
				MobileBy.AndroidUIAutomator(
						"new UiScrollable(new UiSelector()).scrollIntoView("
								+"new UiSelector().text(\""+visibleText+"\"));"));
	}

	public void dealerSelection(String assetType, String assetCategory, String make, String dealer, String source, String DSE, 
			String radioBtn, String loanNo) {

		SwipeOperation sp=new SwipeOperation(driver);
		try {
			assetTypeDrpdwn.click();
			sp.selectValueInList(assetType);
			log.info("Asset type: "+assetType);

			//select asset category 
			assetCategoryDrpdwn.click();
			sp.selectValueInList(assetCategory);
			log.info("Asset category: "+assetCategory);

			//select make 
			makeDrpdwn.click(); 
			//Thread.sleep(2000);
			sp.selectValueInList(make); 
			log.info("Make: "+make);

			//select dealer 
			dealerDrpdwn.click(); 
			sp.selectValueInList(dealer);
			log.info("Dealer: "+dealer);

			//select source 
			sourceDrpdwn.click(); 
			sp.selectValueInList(source);
			log.info("Source: "+source);

			scroll("NEXT");

			//select DSE
			if (DSEDrpdwn.isEnabled()) { 
				DSEDrpdwn.click();
				sp.selectValueInList(DSE); 
				log.info("DSE: "+DSE); 
				//Thread.sleep(2000);
				//custRadioBtn_No.click();	
			} else {
				//custRadioBtn_Yes.click();
				//existingCustIdEle.sendKeys(loanNo);
				log.info("DSE field is disabled"); 
			} 

			custRadioBtn_No.click();

			nextBtn.click();
			Thread.sleep(1000); 
		}
		catch(Exception e) { 
			e.printStackTrace(); 
			log.info("Exception caught "+e); 
		}
	}


}
