package com.ltfs.generics;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;

public class ConfigDataProvider extends BaseLib{

	/**
	 * ConfigDataProvider class will be used to read data from excel sheet dynamically i.e.
	 * in the form of 2D array for different test cases
	 * Here, two different data providers are created to read data from excel
	 * sheet for two test scripts
	 */
	
	public static String testData_filePath = null;	
	
	public static Object[][] getData(String sheetName){
		Object obj[][]=null;

		try {
			log.info("Getting file path");
			log.info(prop.getProperty("filePath"));
			testData_filePath = prop.getProperty("filePath");
			
			FileInputStream fis=new FileInputStream(new File(testData_filePath));
			Workbook wb=WorkbookFactory.create(fis);
			Sheet sh=wb.getSheet(sheetName);

			int rowCount = sh.getLastRowNum();
			int cellCount = sh.getRow(0).getLastCellNum();
			System.out.println("Row count: "+rowCount+" "+"Cell count: "+cellCount);
			log.info("Row count: "+rowCount+" "+"Cell count: "+cellCount);

			obj=new Object[rowCount][cellCount];

			for (int i=1;i<=rowCount;i++) {
				Row rw = sh.getRow(i);
				int cellCt = rw.getLastCellNum();

				if(cellCt==cellCount) {
					for(int j=0;j<cellCt;j++) {

						switch (rw.getCell(j).getCellType()) {
						case STRING:
							obj[i-1][j] = rw.getCell(j).getStringCellValue();
							break;

						case NUMERIC:
							if (DateUtil.isCellDateFormatted(rw.getCell(j))) {
								SimpleDateFormat sdf=new SimpleDateFormat("MMM dd, yyyy");
								obj[i-1][j] = sdf.format(rw.getCell(j).getDateCellValue());
							} 
							else {
								long num = (long) rw.getCell(j).getNumericCellValue();
								obj[i-1][j] = ""+num;
							}
							break;

						default:
							break;
						}
					}
				}
			}
			wb.close();
		}
		
		catch(EncryptedDocumentException e)
		{
			Reporter.log(e.getMessage(),true);
		}
		catch(IOException e)
		{
			Reporter.log(e.getMessage(),true);
		}
		return obj;
	}	


	@DataProvider(name="readValidLogin")
	public Object[][] readValidLoginData() throws Exception{
		Object[][] validLoginData = getData("validLogin");
		return validLoginData;
	}	
	
	@DataProvider(name="readInvalidLogin")
	public Object[][] readInvalidLoginData() throws Exception{
		Object[][] invalidLoginData = getData("invalidLogin");
		return invalidLoginData;
	}

	@DataProvider(name="manualAadharDetails")
	public Object[][] readContactData() throws Exception{
		Object[][] contactData = getData("manualAadhar");
		return contactData;
	}
	
	@DataProvider(name="dealerDetails")
	public Object[][] readDealerData() throws Exception{
		Object[][] dealerData = getData("selectdealer");
		return dealerData;
	}
	
	@DataProvider(name="assetData")
	public Object[][] readAssetData() throws Exception{
		Object[][] assetData = getData("assetDetails");
		return assetData;
	}
	
	@DataProvider(name="applicantInputs")
	public Object[][] readApplicantData() throws Exception{
		Object[][] applicantData = getData("applicantdetails");
		return applicantData;
	}
	
	@DataProvider(name="dashboardInputs")
	public Object[][] readDashboardData() throws Exception{
		Object[][] dashboardData = getData("dashboard");
		return dashboardData;
	}
}
