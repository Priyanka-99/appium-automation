package com.ltfs.generics;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class BaseLib {

	public static final Logger log = Logger.getLogger(BaseLib.class);
	public static AndroidDriver<MobileElement> driver;
	public static Properties prop;
	ExtentReportsClass erc;

	public BaseLib() {

		try {
			prop=new Properties();
			FileInputStream fip = new FileInputStream("./src/resources/config.properties");
			prop.load(fip);
		}
		catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		catch  (IOException e) {
			e.printStackTrace();
		}
	}

	@BeforeMethod
	@Parameters({ "appPackage", "appActivity" })
	public void setUp(String appPackage, String appActivity) throws MalformedURLException, InterruptedException {

		erc=new ExtentReportsClass(driver);
		ExtentReportsClass.startReport("Test1_"+System.currentTimeMillis(), "Android Application");

		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		//Set up desired capabilities and pass the Android app-activity and app-package to Appium
		DesiredCapabilities dc = new DesiredCapabilities();
		dc.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		dc.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		dc.setCapability("appPackage", appPackage);
		dc.setCapability("appActivity", appActivity);
		dc.setCapability("noReset", true);
		dc.setCapability("newCommandTimeout", 6000);
		dc.setCapability("uiautomator2ServerLaunchTimeout", 90000);

		driver = new AndroidDriver<MobileElement>(url, dc);
		Thread.sleep(2000);
		log.info("Application launched");
		erc.Report("Launching the application", "Application launched", "FAIL");

	}

	@AfterMethod 
	public void teardown() { 
		// close the app 
		driver.closeApp();
		log.info("Application closed"); 
	}



}
