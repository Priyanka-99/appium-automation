package com.ltfs.generics;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class SwipeOperation extends BaseLib{

	Logger log = Logger.getLogger(SwipeOperation.class);
	private static AndroidDriver<MobileElement> driver;
	ExtentReportsClass erc;
	
	public SwipeOperation(AndroidDriver<MobileElement> driver) {
		this.driver=driver;
		PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}

	public void selectValueInList(String text){
		try {
			//Thread.sleep(1000);
			//String text = ValueToSelect;

			WebElement listItem = driver.findElement(
					MobileBy.AndroidUIAutomator(
							"new UiScrollable(new UiSelector()).scrollIntoView("
									+"new UiSelector().text(\""+text+"\"));"));

			Thread.sleep(1000);
			listItem.click();


		}catch(Exception e) {
			//Thread.currentThread().stop();
			e.printStackTrace();
		}
	}

	public void selectRadioButton(String ObjectName) { 
		// WebElement element = uf.getObject(ObjectName); 
		List<MobileElement> elemCol = driver.findElementsById(ObjectName);
		if (elemCol.size()!=0){
			if(elemCol.get(0).isDisplayed()){ 
				if(elemCol.get(0).isEnabled()) { 
					String blnSelect = elemCol.get(0).getAttribute("checked");
					if(blnSelect.equalsIgnoreCase("true")) { //
						erc.Report("selectRadioButton","the Object is already Selected-"+ObjectName,"WARNING"); 
						System.out.println("Warning: the Object is already Selected");
					}else { 
						elemCol.get(0).click(); } 
				}else {
					erc.Report("selectRadioButton","Specified object is disabled -"+ObjectName,"FAIL");
					System.out.println("Error: Specified object is disabled - "+ObjectName);
					Thread.currentThread().stop(); 
				} 
			}else {
				erc.Report("selectRadioButton","Specified object is not visible -" +ObjectName,"FAIL");
				System.out.println("Error: Specified object is not visible - "+ObjectName);
				Thread.currentThread().stop(); 
			} 
		}else {
			erc.Report("selectRadioButton","No Object found with the objec name -" +ObjectName,"FAIL");
			System.out.println("Error: No Object found with the object name - "+ObjectName); 
			Thread.currentThread().stop(); 
		} 
	}


}
