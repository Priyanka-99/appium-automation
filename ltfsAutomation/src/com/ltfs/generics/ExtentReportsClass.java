package com.ltfs.generics;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

//It is possible to attach screenshots. To add a screen-shot, simply call addScreenCapture. 
//This method returns the HTML with  tag which can be used anywhere in the log details.

public class ExtentReportsClass extends BaseLib {
	public ExtentReportsClass(AndroidDriver<MobileElement> driver) {
		super();
	}
	public static ExtentReports extent ;
	public static ExtentTest logger;
	public static ThreadLocal<ExtentTest> extentTest=new ThreadLocal<ExtentTest>();
	String ReportName ="MyReport";
	String BrowserName = "Chrome";	

	@BeforeMethod
	public static void startReport(String ReportName, String BrowserName){
		createFolder();
		String reportPath ="result//"+ThreadLocalVariables.getScreenShortFolder()+"//"+Thread.currentThread().getId()+"_"+ReportName+".html";
		ThreadLocalVariables.setExtendReportFilePath(reportPath);
		extent = new ExtentReports (reportPath, true);
		extent
		.addSystemInfo("Host Name", "Mobile Testing")
		.addSystemInfo("Environment", "Automation Testing")
		.addSystemInfo("User Name", "Kishore");
		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
	}

	public static void createFolder() {
		String fileName = "Test_"+Thread.currentThread().getName()+"_"+System.currentTimeMillis();
		ThreadLocalVariables.setScreenShortFolder(fileName);
		String Path = System.getProperty("user.dir")+"\\result\\"+ThreadLocalVariables.getScreenShortFolder();
		File newDir = new File(Path);
		newDir.mkdir();
	}

	public String getScreenhot(String Status) throws Exception {
		String destination = null;
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		if(Status.equalsIgnoreCase("PASS")) {
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Pass_"+System.currentTimeMillis()+".png";
		}else if(Status.equalsIgnoreCase("FAIL")){
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Fail_"+System.currentTimeMillis()+".png";
		}else if(Status.equalsIgnoreCase("INFO")){
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Info_"+System.currentTimeMillis()+".png";
		}else if(Status.equalsIgnoreCase("WARNING")){
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Warning_"+System.currentTimeMillis()+".png";
		}else if(Status.equalsIgnoreCase("ERROR")){
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Error_"+System.currentTimeMillis()+".png";
		}else {
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//"+System.currentTimeMillis()+".png";
		}
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}

	@AfterMethod
	public static void endreport() {
		extent.flush();
	}

	public void Report(String TCNAME, String TCDES, String Status)  {
		try {
			ExtentReportsClass ex =new ExtentReportsClass(driver);
			if(Status.equalsIgnoreCase("PASS")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.PASS, TCDES);
				String screenshotPath =ex.getScreenhot("PASS");
				extentTest.get().log(LogStatus.PASS, logger.addScreenCapture(screenshotPath));
				ExtentReportsClass.endreport();
			}else if (Status.equalsIgnoreCase("FAIL")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.FAIL, TCDES);
				String screenshotPath = ex.getScreenhot("FAIL");
				extentTest.get().log(LogStatus.FAIL, logger.addScreenCapture(screenshotPath));
				ExtentReportsClass.endreport();
			}else if (Status.equalsIgnoreCase("INFO")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.INFO, TCDES);
				String screenshotPath = ex.getScreenhot("INFO");
				extentTest.get().log(LogStatus.INFO, logger.addScreenCapture(screenshotPath));
				ExtentReportsClass.endreport();
			}else if (Status.equalsIgnoreCase("WARNING")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.WARNING, TCDES);
				String screenshotPath = ex.getScreenhot("WARNING");
				extentTest.get().log(LogStatus.WARNING, logger.addScreenCapture(screenshotPath));
				ExtentReportsClass.endreport();
			}else if (Status.equalsIgnoreCase("ERROR")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.ERROR, TCDES);
				String screenshotPath = ex.getScreenhot("ERROR");
				extentTest.get().log(LogStatus.ERROR, logger.addScreenCapture(screenshotPath));
				ExtentReportsClass.endreport();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void Report(String TCNAME, String TCDES, String Status, boolean TakeScreenShort)  {
		try {
			ExtentReportsClass ex =new ExtentReportsClass(driver);
			if(Status.equalsIgnoreCase("PASS")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.PASS, TCDES);
				if(TakeScreenShort == true) {
					String screenshotPath =ex.getScreenhot("PASS");
					extentTest.get().log(LogStatus.PASS, logger.addScreenCapture(screenshotPath));
				}
				ExtentReportsClass.endreport();
			}else if (Status.equalsIgnoreCase("FAIL")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.FAIL, TCDES);
				if(TakeScreenShort == true) {
					String screenshotPath = ex.getScreenhot("FAIL");
					extentTest.get().log(LogStatus.FAIL, logger.addScreenCapture(screenshotPath));
				}
				ExtentReportsClass.endreport();
			}else if (Status.equalsIgnoreCase("INFO")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.INFO, TCDES);
				ExtentReportsClass.endreport();
			}else if (Status.equalsIgnoreCase("WARNING")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.WARNING, TCDES);
				if(TakeScreenShort == true) {
					String screenshotPath = ex.getScreenhot("WARNING");
					extentTest.get().log(LogStatus.WARNING, logger.addScreenCapture(screenshotPath));
				}
				ExtentReportsClass.endreport();
			}else if (Status.equalsIgnoreCase("ERROR")) {
				logger = ExtentReportsClass.extent.startTest(TCNAME);
				extentTest.set(logger);
				extentTest.get().log(LogStatus.ERROR, TCDES);
				if(TakeScreenShort == true) {
					String screenshotPath = ex.getScreenhot("ERROR");
					extentTest.get().log(LogStatus.ERROR, logger.addScreenCapture(screenshotPath));
				}
				ExtentReportsClass.endreport();
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}



}