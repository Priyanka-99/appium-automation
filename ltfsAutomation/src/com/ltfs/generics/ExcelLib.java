package com.ltfs.generics;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.Reporter;

public class ExcelLib extends BaseLib{

	String filePath;

	public ExcelLib(String filePath) {
		this.filePath = filePath;
		log.info("Getting file path");
		log.info(prop.getProperty("filePath"));
	}

	public String readData(String sheetName, int row, int cell) {
		String value = null;

		try
		{
			//testData_filePath = prop.getProperty("filePath");
			FileInputStream fis=new FileInputStream(new File(filePath));
			Workbook wb = WorkbookFactory.create(fis);
			Cell cl = wb.getSheet(sheetName).getRow(row).getCell(cell);

			switch (cl.getCellType()) {
			case STRING:
				value = cl.getStringCellValue();
				break;

			case NUMERIC:
				if (DateUtil.isCellDateFormatted(cl)) {
					SimpleDateFormat sdf=new SimpleDateFormat("MMM dd, yyyy");
					value = sdf.format(cl.getDateCellValue());
				}
				else {
					long num = (long) cl.getNumericCellValue();
					value = ""+num;
				}
				break;

			default:
				break;
			}
			wb.close();
		}
		catch(EncryptedDocumentException e)
		{
			Reporter.log(e.getMessage(),true);
		}
		catch(IOException e)
		{
			Reporter.log(e.getMessage(),true);
		}

		return value;
	}


}
