package com.ltfs.generics;

public class ThreadLocalVariables {
	public static ThreadLocal<String> TC_ID = new ThreadLocal<String>();
	public static ThreadLocal<String> TC_NAME = new ThreadLocal<String>();
	public static ThreadLocal<String> ApplicationName = new ThreadLocal<String>();
	public static ThreadLocal<String> ScreenShortFolder = new ThreadLocal<String>();
	public static ThreadLocal<String> ExtendReportFilePath = new ThreadLocal<String>();
	public static ThreadLocal<String> ExecuteFlag = new ThreadLocal<String>();
	public static ThreadLocal<String> SheetName = new ThreadLocal<String>();
//	public static ThreadLocal<String> SRUWPortal = new ThreadLocal<String>();
//	public static ThreadLocal<ExtentReports> extent =new ThreadLocal<ExtentReports>();
//	public static ThreadLocal<ExtentTest> logger= new ThreadLocal<ExtentTest>();
	
	public static void setTCID(String TCID) {
		TC_ID.set(TCID);
	}
	public static void setTCName(String TCName) {
		TC_NAME.set(TCName);
	}
	public static String getTCID() {
		return TC_ID.get();
	}
	public static String getTCName() {
		return TC_NAME.get();
	}
	public static void setExtendReportFilePath(String filePath) {
		ExtendReportFilePath.set(filePath);
	}
	public static String getExtendReportFilePath() {
		return ExtendReportFilePath.get();
	}
	public static void setScreenShortFolder(String folderName) {
		ScreenShortFolder.set(folderName);
	}
	public static String getScreenShortFolder() {
		return ScreenShortFolder.get();
	}
	public static void setExecuteFlag(String folderName) {
		ExecuteFlag.set(folderName);
	}
	public static String getExecuteFlag() {
		return ExecuteFlag.get();
	}
	public static void setSheetName(String folderName) {
		SheetName.set(folderName);
	}
	public static String getSheetName() {
		return SheetName.get();
	}


}
