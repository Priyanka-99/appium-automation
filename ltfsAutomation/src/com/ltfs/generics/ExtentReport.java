package com.ltfs.generics;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.xml.XmlSuite;

import com.google.common.io.Files;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;

public class ExtentReport implements IReporter{

	private ExtentReports extent;
	AndroidDriver<MobileElement> driver;

	//AdminLogin al = new AdminLogin();
	public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites, String outputDirectory) {
		extent = new ExtentReports(outputDirectory + File.separator+"LTFSTestReport.html", true);

		for (ISuite suite : suites) {
			Map<String, ISuiteResult> result = suite.getResults();

			for (ISuiteResult r : result.values()) {
				ITestContext context = r.getTestContext();

				try {
					buildTestNodes(context.getPassedTests(), LogStatus.PASS);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					buildTestNodes(context.getFailedTests(), LogStatus.FAIL);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					buildTestNodes(context.getSkippedTests(), LogStatus.SKIP);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		extent.flush();
		extent.close();
	}

	private void buildTestNodes(IResultMap tests, LogStatus status) throws IOException {
		ExtentTest test;
		if (tests.size() > 0) {
			for (ITestResult result : tests.getAllResults()) {
				test = extent.startTest(result.getMethod().getMethodName());

				test.setStartedTime(getTime(result.getStartMillis()));
				test.setEndedTime(getTime(result.getEndMillis()));

				for (String group : result.getMethod().getGroups())
					test.assignCategory(group);

				if (result.getThrowable() != null) {
					test.log(status, result.getThrowable().getMessage());
					//String screenshotPath =ex.getScreenhot("PASS");
					//test.log(LogStatus.PASS, logger.addScreenCapture(screenshotPath));
				} else {
					test.log(status, " Test " + status.toString().toLowerCase()
							+ "ed");
				}

				extent.endTest(test);
			}
		}
	}

	private Date getTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}
	
	public String getScreenhot(String Status) throws Exception {
		String destination = null;
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		if(Status.equalsIgnoreCase("PASS")) {
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Pass_"+System.currentTimeMillis()+".png";
		}else if(Status.equalsIgnoreCase("FAIL")){
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Fail_"+System.currentTimeMillis()+".png";
		}else if(Status.equalsIgnoreCase("INFO")){
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Info_"+System.currentTimeMillis()+".png";
		}else if(Status.equalsIgnoreCase("WARNING")){
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Warning_"+System.currentTimeMillis()+".png";
		}else if(Status.equalsIgnoreCase("ERROR")){
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//Error_"+System.currentTimeMillis()+".png";
		}else {
			destination = System.getProperty("user.dir")+"//result//"+ThreadLocalVariables.getScreenShortFolder()+"//"+System.currentTimeMillis()+".png";
		}
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);
		return destination;
	}
}

